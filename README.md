# ID4me Backend Integration Demo

ID4me backend integration demo.

## Author

Sven Woltmann, CTO of [Fonpit AG](https://fonpit.com/), operator of [AndroidPIT](https://www.androidpit.com), the world's largest multi-lingual Android community.

## Links

* ID4me client library:<br>
https://mvnrepository.com/artifact/org.id4me/relying-party-api

* ID4me client source code:<br>
https://gitlab.com/ID4me/RelyingPartyApi

* ID4me programmer's guide:<br>
http://id4me.org/files/rp-api/java/1.0/ID4me-JavaRelyingPartyApi-1.0.ProgrammersGuide.pdf

## Lessons Learned

1. In a multi-domain environment you need one configuration and one _ID4meLogon_ instance per domain. Each configuration must have a distinct _registrationDataPath_. Otherwise configurations will get mixed up.

2. Ignore _boolean_ return values, they are always _true_. Catch and handle exceptions instead.

3. Handle unverified e-mail addresses appropriately. Verify them by sending a verification link or code or&mdash;if the user wants to connect to an existing account&mdash;ask the user for the e-mail address and password of that account.

4. Compare the requested with the returned ID4me identifier. If they are different, you might want to ask the user if he/she wants to continue or start over.

5. Either don't set the _essential_ flag in claims or&mdash;if you want to use this flag&mdash;handle exceptions in _Id4meLogon.userinfo(..)_ appropriately and ask the user to enter the essential information in her/his ID4me account.

6. The library is not yet thread safe. Synchronize the API calls externally! Even for calls on different _ID4meLogon_ instances. (They access and modify shared data structures without proper synchronization.)
