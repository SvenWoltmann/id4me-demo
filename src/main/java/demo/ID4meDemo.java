package demo;

import org.id4me.Id4meLogon;
import org.id4me.Id4meSessionData;
import org.id4me.config.Id4meClaimsParameters;
import org.id4me.config.Id4meProperties;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public class ID4meDemo {

    public static void main(String[] args) throws Exception {
        Id4meProperties properties = new Id4meProperties()
                .setRegistrationDataPath(new File(System.getProperty("java.io.tmpdir"),"id4me-demo-reg-data").toString())
                .setLogoURI("https://www.androidpit.com/img/logo/favicon.png")
                .setRedirectURI("https://www.androidpit.com/id4me/demo-callback")
                .setClientName("ID4me Demo")
                .setDnsResolver("8.8.8.8")
                .setDnssecRequired(false);

        Id4meClaimsParameters claimsParameters = new Id4meClaimsParameters()
                .addEntry(new Id4meClaimsParameters.Entry()
                        .setName("email")
                        .setEssential(true)
                        .setReason("Needed to create the profile"))
                .addEntry(new Id4meClaimsParameters.Entry()
                        .setName("name")
                        .setReason("Displayname in the user data"))
                .addEntry(new Id4meClaimsParameters.Entry()
                        .setName("given_name"));

        System.out.println("Creating Id4meLogon...");
        Id4meLogon logon = new Id4meLogon(properties, claimsParameters);

        System.out.print("\nPlease enter your ID4me identifier: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String identifier = br.readLine();

        System.out.println("\nCreating session data...");
        Id4meSessionData sessionData = logon.createSessionData(identifier, true);

        System.out.println("\nBuilding authorization URL...");
        String authorizationURL = logon.authorize(sessionData);

        System.out.println("\nauthorizationURL = " + authorizationURL);

        System.out.print("\nPlease enter the code: ");
        String code = br.readLine();

        System.out.println("\nVerifying code...");
        logon.authenticate(sessionData, code);

        System.out.println("\nRetrieving user info...");
        logon.userinfo(sessionData);

        JSONObject userinfo = sessionData.getUserinfo();
        System.out.println("\nuserinfo = " + userinfo.toString(4));
    }

}
